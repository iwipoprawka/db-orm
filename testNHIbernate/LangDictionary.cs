﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Linq;
using testNHIbernate.Domain;

namespace testNHIbernate
{
    class LangDictionary<T>
    {
        public void Add(T entry)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Save(entry);
                    transaction.Commit();
                }
            }
        }

        public List<T> GetAll()
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                var result = session.Query<T>().ToList<T>();
                return result;
            }
        }

        public void AddOrUpdate(T entry)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.SaveOrUpdate(entry);
                    transaction.Commit();
                }
            }
        }

        public void Update(T entry)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Update(entry);
                    transaction.Commit();
                }
            }
        }

        public void ClearAll()
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.CreateQuery("DELETE FROM " + typeof(T).Name).ExecuteUpdate();
                    transaction.Commit();
                }
            }
        }

    }
}
