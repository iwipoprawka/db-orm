﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHibernate;
using NHibernate.Cfg;
using testNHIbernate.Domain;
using NHibernate.Tool.hbm2ddl;
using System.IO;
using System.Media;
using testNHIbernate.Wiktionary;
using System.Data.SqlServerCe;
using testNHIbernate.Ngram;
using System.Threading;
using System.Windows.Forms.DataVisualization.Charting;

namespace testNHIbernate
{
    public partial class Form1 : Form
    {
        LangDictionary<LangDictionaryEntry> dictionary = new LangDictionary<LangDictionaryEntry>();
        
        private static Form1 Instance = null;
        public Thread upData, cNG, cNGIPA;
        public Form1()
        {
            if (Instance != null)
            {
                return;
            }
            InitializeComponent();
            Instance = this;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        //Show Database
        private void showDataBase_Click(object sender, EventArgs e)
        {
            consoleTextBox.Clear();
            List<LangDictionaryEntry> words = dictionary.GetAll();

            StringBuilder sb = new StringBuilder();
            sb.Append("Język | słowo | wymowa");
            sb.Append("\r\n");
            foreach (LangDictionaryEntry w in words)
            {
                sb.Append(w.jezyk + ": " + w.slowo + " " +w.wymowa);
                sb.Append("\r\n");
            }
            consoleTextBox.Text = sb.ToString();
        }

        private void updateDatabaseButton_Click(object sender, EventArgs e)
        {
            //
            // Open dialog for choosing Wiktionary XML dump file
            //
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = "XML Files|*.xml";
            ofd.Multiselect = false;
            string xmlFilePath;
            DialogResult openDialogResult = ofd.ShowDialog();

            if (openDialogResult == DialogResult.OK)
            {
                xmlFilePath = ofd.FileName;
            }
            else
            {
                DebugPrint("Dialog error: " + ofd.ToString());
                return;
            }

            //
            // Create dialog to write specify language to update
            //
            LanguageDialog languageDialog = new LanguageDialog();
            DialogResult languageDialogResult = languageDialog.ShowDialog();

            string language;

            if (languageDialogResult == DialogResult.OK)
            {
                language = languageDialog.Language;
            }
            else
            {
                return;
            }

            upData = new Thread(() => updateDatabase(xmlFilePath, language));
            upData.Start();
        }

        private void updateDatabase(string wikiDumpFilePath, string language)
        {
            WiktionaryParser parser = new WiktionaryParser(wikiDumpFilePath);
            parser.Parse(language, dictionary);
        }

        private void createNGram_Click(object sender, EventArgs e)
        {
            NGram ngrams = new NGram();
            NGramIPA ngramsIPA = new NGramIPA();

            cNG = new Thread(() => createNG(ngrams, ngramsIPA));
            cNG.Start();
        }

        private void createNG(NGram ngrams, NGramIPA ngramsIPA)
        {
            DebugPrint("Getting all words...");

            List<LangDictionaryEntry> words = dictionary.GetAll();

            DebugPrint("Generating ngrams...");

            foreach (LangDictionaryEntry w in words)
            {
                ngrams.AddToNGram(w.slowo, w.jezyk);
                if (!(w.wymowa == string.Empty || w.wymowa == null))
                    ngramsIPA.AddToNGram(w.wymowa, w.jezyk);
            }

            DebugPrint("Saving ngrams in database...");

            ngrams.SaveToDatabase();
            ngramsIPA.SaveToDatabase();
            DebugPrint("Creating distance matrix...");
            ngrams.CreateDistanceMatrix();
            DebugPrint("Creating distance matrix for IPA...");
            ngramsIPA.CreateDistanceMatrix();
            DebugPrint("Creating ngram view...");
            
            List<string> languageList = ngrams.GetAllUniqueLanguages();
            ClearTabView();
            CreateNgramChartTabs(ngrams, languageList);
            CreateNgramDistanceTable(ngrams, languageList);
            List<string> ipaLanguageList = ngramsIPA.GetAllUniqueLanguages();
            CreateNgramChartTabsIPA(ngramsIPA, ipaLanguageList);
            CreateNgramDistanceTableIPA(ngramsIPA, ipaLanguageList);

            Form1.DebugPrint("Done.");
        }

        public static void DebugPrint(string s)
        {
            try
            {
                Instance.Invoke((MethodInvoker)delegate {
                
                    Instance.consoleTextBox.AppendText(s);
                    Instance.consoleTextBox.AppendText("\r\n");
                    Instance.consoleTextBox.SelectionStart = Instance.consoleTextBox.Text.Length;
                    Instance.consoleTextBox.ScrollToCaret();
                });
            }
            catch (Exception e)
            {
                // Do nothing.
            }

        }

        private void ClearTabView()
        {
            Invoke((MethodInvoker)delegate
            {
                ngramTabView.TabPages.Clear();
            });
        }

        private void CreateNgramChartTabs(NGram ngrams, List<string> languageList)
        {
            foreach (string language in languageList)
            {
                List<NGramEntry> ngramList = ngrams.GetAllSorted(language);
                Invoke((MethodInvoker)delegate
                {
                    ngramTabView.TabPages.Add(language, "N-gram: " + language);
                    Chart chart = new Chart();
                    ChartArea chartArea = new ChartArea();
                    chart.ChartAreas.Add(chartArea);
                    chart.Dock = DockStyle.Fill;

                    foreach (NGramEntry ngramEntry in ngramList)
                    {
                        Series series = chart.Series.Add(ngramEntry.Ngram);
                        series.Points.Add(ngramEntry.Count);
                        series.AxisLabel = ngramEntry.Ngram;
                    }

                    chart.Titles.Add(language);
                    ngramTabView.TabPages[language].Controls.Add(chart);
                });
            }
        }

        private void CreateNgramDistanceTable(NGram ngrams, List<string> languageList)
        {
            Invoke((MethodInvoker)delegate
            {
                DataGridView langDataGridView = new DataGridView();

                //
                // Create columns with language headers
                //
                foreach (string language in languageList)
                {
                    langDataGridView.Columns.Add(language, language);
                }

                //
                // Add row for each language
                //
                foreach (string rowLanguage in languageList)
                {
                    List<object> rowData = new List<object>();
                    foreach (string columnLanguage in languageList)
                    {
                        rowData.Add(ngrams.languageMatrix[new Tuple<string, string>(columnLanguage,rowLanguage)]);
                    }
                    int rowId = langDataGridView.Rows.Add(rowData.ToArray());
                    langDataGridView.Rows[rowId].HeaderCell.Value = rowLanguage;
                }

                //
                // Create new tab and add language grid view to that tab
                //
                ngramTabView.TabPages.Add("Data", "Language Distances");
                langDataGridView.Dock = DockStyle.Fill;
                langDataGridView.ReadOnly = true;
                langDataGridView.AllowUserToAddRows = false;
                langDataGridView.AllowUserToDeleteRows = false;
                langDataGridView.AllowUserToResizeRows = false;
                langDataGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
                ngramTabView.TabPages["Data"].Controls.Add(langDataGridView);
                
            });
        }



        private void CreateNgramChartTabsIPA(NGramIPA ngramsIPA, List<string> languageList)
        {
            foreach (string language in languageList)
            {
                List<NGramEntryIPA> ngramList = ngramsIPA.GetAllSorted(language);
                Invoke((MethodInvoker)delegate
                {
                    ngramTabView.TabPages.Add(language+"IPA", "N-gram IPA: " + language);
                    Chart chart = new Chart();
                    ChartArea chartArea = new ChartArea();
                    chart.ChartAreas.Add(chartArea);
                    chart.Dock = DockStyle.Fill;

                    foreach (NGramEntryIPA ngramEntry in ngramList)
                    {
                        Series series = chart.Series.Add(ngramEntry.NgramIPA);
                        series.Points.Add(ngramEntry.Count);
                        series.AxisLabel = ngramEntry.NgramIPA;
                    }

                    chart.Titles.Add(language);
                    ngramTabView.TabPages[language + "IPA"].Controls.Add(chart);
                });
            }
        }

        private void CreateNgramDistanceTableIPA(NGramIPA ngramsIPA, List<string> languageList)
        {
            Invoke((MethodInvoker)delegate
            {
                DataGridView langDataGridView = new DataGridView();

                //
                // Create columns with language headers
                //
                foreach (string language in languageList)
                {
                    langDataGridView.Columns.Add(language, language);
                }

                //
                // Add row for each language
                //
                foreach (string rowLanguage in languageList)
                {
                    List<object> rowData = new List<object>();
                    foreach (string columnLanguage in languageList)
                    {
                        rowData.Add(ngramsIPA.languageMatrix[new Tuple<string, string>(columnLanguage, rowLanguage)]);
                    }
                    int rowId = langDataGridView.Rows.Add(rowData.ToArray());
                    langDataGridView.Rows[rowId].HeaderCell.Value = rowLanguage;
                }

                //
                // Create new tab and add language grid view to that tab
                //
                ngramTabView.TabPages.Add("DataIPA", "Language Distances IPA");
                langDataGridView.Dock = DockStyle.Fill;
                langDataGridView.ReadOnly = true;
                langDataGridView.AllowUserToAddRows = false;
                langDataGridView.AllowUserToDeleteRows = false;
                langDataGridView.AllowUserToResizeRows = false;
                langDataGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
                ngramTabView.TabPages["DataIPA"].Controls.Add(langDataGridView);

            });
        }
    }
}