﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json;
using testNHIbernate.Domain;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace testNHIbernate.Wiktionary
{
    class WiktionaryParser
    {
        private class Section
        {
            public string Name { get; set; }
            public int Depth { get; set; }
            public string Content { get; set; }

            public override string ToString()
            {
                return Name;
            }
        }

        private string XmlFilePath;

        //
        // Empty constructor
        //
        public WiktionaryParser(string xmlFilePath)
        {
            XmlFilePath = xmlFilePath;
        }

        //
        // Parse XML dump from Wiktionary
        //
        public void Parse(string language, LangDictionary<LangDictionaryEntry> dictionary)
        {
            Form1.DebugPrint("Starting parsing (" + language + ") language.");

            //
            // Test to see if file is present
            //
            if (!File.Exists(XmlFilePath))
            {
                Form1.DebugPrint("XML file to parse does not exist.");
                return;
            }

            if (language == null || language == "")
            {
                Form1.DebugPrint("Language name incorrect.");
                return;
            }

            //
            // Iterate through pages
            //
            Parallel.ForEach(GetPages(), page =>
            {
                try
                {
                    //
                    // Namespace == 0 => Regular article
                    //
                    if (GetChildNode(page, "ns").InnerText != "0")
                    {
                        return;
                    }

                    //
                    // Get language section
                    //
                    Section langSection = GetLanguageSection(page, language);

                    if (langSection == null)
                    {
                        return;
                    }

                    LangDictionaryEntry langEntry = new LangDictionaryEntry();
                    langEntry.jezyk = language;
                    langEntry.slowo = GetChildNode(page, "title").InnerText;

                    //
                    // Get pronunciation section
                    //
                    Section pronunciationSection = GetSubsection("Pronunciation", langSection, page);

                    if (pronunciationSection != null)
                    {
                        //
                        // Parse IPA
                        //
                        string ipaNotation = GetIpaNotation(pronunciationSection);

                        if (ipaNotation != null)
                        {
                            langEntry.wymowa = ipaNotation;
                        }
                    }

                    ////
                    //// Get sound file name
                    ////
                    //string soundFileName = GetSoundFileName(pronunciationSection);

                    //if (soundFileName != null)
                    //{
                    //    //
                    //    // Download sound file
                    //    //
                    //    langEntry.dzwiek = DownloadFile(soundFileName);
                    //}

                    //
                    // Insert object to database
                    //
                    dictionary.AddOrUpdate(langEntry);
                }
                catch (Exception ex)
                {
                    Form1.DebugPrint(ex.Message);
                }
            });
        }

        //
        // Page structure described as schema: https://www.mediawiki.org/xml/export-0.8.xsd
        //
        private XmlNode ParsePage(XmlReader reader)
        {
            XmlDocument page = new XmlDocument();
            XmlNode node = page.ReadNode(reader);
            return node;
        }

        //
        // XmlNode.ChildNode search helper
        //
        private XmlNode GetChildNode(XmlNode pageNode, string childName)
        {
            foreach (XmlNode childNode in pageNode.ChildNodes)
            {
                if (childNode.Name == childName)
                {
                    return childNode;
                }
            }

            return null;
        }

        //
        // Enumerate pages through LINQ
        //
        private IEnumerable<XmlNode> GetPages()
        {
            using (XmlReader reader = XmlReader.Create(XmlFilePath))
            {
                reader.MoveToContent();
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == "page")
                        {
                            XmlNode node = null;
                            try
                            {
                                node = ParsePage(reader);
                            } catch (Exception e)
                            {

                            }

                            if (node != null)
                            {
                                yield return node;
                            }
                        }
                    }
                }
            }
        }

        //
        // Return page node with given title
        //
        private XmlNode GetPageByTitle(string title)
        {
            foreach (XmlNode page in GetPages())
            {
                if (GetChildNode(page, "title").InnerText == title)
                {
                    return page;
                }
            }

            return null;
        }

        //
        // Return page wikicontent
        //
        private string GetPageContent(XmlNode page)
        {
            XmlNode revisionNode = GetChildNode(page, "revision");
            if (revisionNode != null)
            {
                XmlNode textNode = GetChildNode(revisionNode, "text");
                return textNode.InnerText;
            }
            return null;
        }

        //
        // Return all page links as page names to which they are related to
        //
        private List<string> GetPageLinks(XmlNode page)
        {
            string content = GetPageContent(page);
            Regex linkRegex = new Regex("\\[\\[(.*?)\\|(.*?)\\]\\]");
            List<string> linkList = new List<string>();
            foreach(Match match in linkRegex.Matches(content))
            {
                linkList.Add(match.Groups[1].Value);
            }

            return linkList;

        }

        //
        // Interpret index page
        //
        private void ParseIndexPage(XmlNode page, string language)
        {
            IEnumerable<string> links = GetPageLinks(page).Where(link => link.StartsWith("Index:" + language));

            foreach(string link in links)
            {
                XmlNode letterPage = GetPageByTitle(link);
                if (letterPage != null)
                {
                    ParseLetterPage(letterPage);
                }
            }
        }

        //
        // Interpret letter page
        //
        private void ParseLetterPage(XmlNode page)
        {
            Form1.DebugPrint("Parsing letter page: " + page.Name);
        }

        //
        // Get sections
        //
        private List<Section> GetSections(XmlNode page)
        {
            string pageContent = GetPageContent(page);
            List<Section> sectionList = new List<Section>();
            Queue<int> sectionStartList = new Queue<int>();

            Regex sectionRegex = new Regex(@"=+(.+?)=+\n");
            MatchCollection sections = sectionRegex.Matches(pageContent);

            // Setup section info
            foreach (Match sectionMatch in sections)
            {
                Section section = new Section();
                section.Depth = sectionMatch.Groups[1].Index - sectionMatch.Index;
                section.Name = sectionMatch.Groups[1].Value;
                sectionList.Add(section);
                sectionStartList.Enqueue(sectionMatch.Index);
            }

            sectionStartList.Enqueue(pageContent.Length);

            for (int i = 0; i < sectionList.Count; i++)
            {
                Section section = sectionList[i];
                int firstIndex = sectionStartList.First() + section.Depth * 2 + section.Name.Length + 1;
                string sectionContent = pageContent.Substring(firstIndex, sectionStartList.ElementAt(1) - firstIndex);
                section.Content = sectionContent;
                sectionStartList.Dequeue();
            }

            return sectionList;
        }

        //
        // Get language section
        //
        private Section GetLanguageSection(XmlNode page, string languageName)
        {
            List<Section> sections = GetSections(page);
            
            foreach (Section section in sections)
            {
                if (section.Name == languageName)
                {
                    return section;
                }
            }

            return null;
        }

        //
        // TODO: Rework to be able to find language and pronounciation sections using two different functions
        //
        private Section GetSubsection(string subsectionName, Section parentSection, XmlNode page)
        {
            bool parentSectionFound = false;
            List<Section> sections = GetSections(page);

            foreach (Section section in sections)
            {
                if (parentSectionFound)
                {
                    if (section.Depth == parentSection.Depth)
                    {
                        return null;
                    }
                    else if (section.Name == subsectionName)
                    {
                        return section;
                    }
                }

                if (section.Name == parentSection.Name)
                {
                    parentSectionFound = true;
                }
            }

            return null;
        }

        //
        // Get first IPA notation of pronounciation from given section
        //
        private string GetIpaNotation(Section section)
        {
            Regex wikiIpaRegex = new Regex("{{IPA\\|(.*?)\\|(.*?)}}");
            Match match = wikiIpaRegex.Match(section.Content);

            if (match != null)
            {
                return match.Groups[1].Value;
            }
            else
            {
                return null;
            }
        }

        //
        // Get first sound file name from given section
        //
        private string GetSoundFileName(Section section)
        {
            Regex wikiIpaRegex = new Regex("{{audio\\|(.*?)\\|(.*?)\\|(.*?)}}");
            Match match = wikiIpaRegex.Match(section.Content);

            if (match != null)
            {
                return match.Groups[1].Value;
            }
            else
            {
                return null;
            }
        }

        //
        // Download file
        //
        private byte[] DownloadFile(string url)
        {
            byte[] result = null;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var response = client.GetAsync(url).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsByteArrayAsync().Result;
                    }
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }
    }
}
