﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testNHIbernate.Wiktionary
{
    public partial class LanguageDialog : Form
    {
        public string Language = null;

        public LanguageDialog()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            Language = languageTextBox.Text;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Language = null;
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
