﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testNHIbernate.Domain
{
    public class NGramEntry
    {
        public virtual int Id { get; set; }
        public virtual string Language { get; set; }
        public virtual string Ngram { get; set; }
        public virtual int Count { get; set; }
    }

    public class NGramEntryIPA
    {
        public virtual int Id { get; set; }
        public virtual string Language { get; set; }
        public virtual string NgramIPA { get; set; }
        public virtual int Count { get; set; }
    }
}
