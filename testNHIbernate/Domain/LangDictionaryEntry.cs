﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace testNHIbernate.Domain
{
    [Serializable]
    class LangDictionaryEntry
    {
        public virtual string jezyk { get; set; }
        public virtual string slowo { get; set; }
        public virtual string wymowa { get; set; }
        public virtual byte[] dzwiek { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            LangDictionaryEntry id;
            id = (LangDictionaryEntry)obj;
            if (id == null)
                return false;
            if (jezyk == id.jezyk &&
            slowo == id.slowo)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            return (jezyk + "|" + slowo).GetHashCode();
        }
    }
}
