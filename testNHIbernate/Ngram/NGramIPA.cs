﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using NHibernate.Criterion;
using testNHIbernate.Domain;

namespace testNHIbernate.Ngram
{
    public class NGramIPA
    {
        int _startOrder = 2;
        int _maxOrder = 2;

        Dictionary<string, Dictionary<string, int>> ngrams = new Dictionary<string, Dictionary<string, int>>();
        LangDictionary<NGramEntryIPA> ngram = new LangDictionary<NGramEntryIPA>();
        public Dictionary<Tuple<string, string>, float> languageMatrix = new Dictionary<Tuple<string, string>, float>();

        public void AddToNGram(string word, string language)
        {

            for (int order = _startOrder; order <= _maxOrder; ++order)
            {
                for (int i = 0; i < (word.Length - order + 1); i++)
                {
                    string key = word.Substring(i, order);
                    key = key.ToLower();

                    if (!ngrams.ContainsKey(language))
                    {
                        ngrams.Add(language, new Dictionary<string, int>());
                    }
                    else
                    {
                        if (!ngrams[language].ContainsKey(key))
                            ngrams[language].Add(key, 1);
                        else
                            ngrams[language][key] += 1;
                    }
                }
            }


        }

        public List<NGramEntryIPA> GetAllSorted(string language)
        {
            List<NGramEntryIPA> result = ngram.GetAll().FindAll(entry => entry.Language == language);
            result.Sort((ngram1, ngram2) => ngram1.NgramIPA.CompareTo(ngram2.NgramIPA));
            return result;
        }

        public List<NGramEntryIPA> GetAllSortedByCounts(string language)
        {
            List<NGramEntryIPA> result = ngram.GetAll().FindAll(entry => entry.Language == language);
            result.Sort((ngram1, ngram2) => -1 * ngram1.Count.CompareTo(ngram2.Count));
            return result;
        }

        public List<string> GetAllUniqueLanguages()
        {
            List<string> lang = ngram.GetAll().Select(entry => entry.Language).Distinct().ToList();
            return lang;
        }

        public void setMin(int i)
        {
            _startOrder = i;
        }

        public void setMax(int i)
        {
            _maxOrder = i;
        }

        public void SaveToDatabase()
        {
            Form1.DebugPrint("Save to Database initiated");
            ngram.ClearAll();

            try
            {
                foreach (var gram in ngrams)
                {
                    foreach (var i in gram.Value)
                    {
                        NGramEntryIPA entry = new NGramEntryIPA() { Language = gram.Key, NgramIPA = i.Key, Count = i.Value };
                        ngram.Add(entry);
                        //Form1.DebugPrint("Executing insert for: " + gram.Key);
                    }
                }

            }
            catch (Exception e)
            {
                Form1.DebugPrint("Fail to save in the database");
            }
            finally
            {
                Form1.DebugPrint("Connection closed");
            }

        }

        public void SaveToCSV()
        {
            string filePath = @"C:\Downloads\test.csv";
            string delimiter = ",";

            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
            }
            else
            {
                File.Delete(filePath);
                File.Create(filePath).Close();
            }
            StringBuilder sb = new StringBuilder();

            foreach (var ngram in ngrams)
            {
                foreach (var i in ngram.Value)
                {
                    sb.AppendLine(ngram.Key + "," + i.Key + "," + i.Value);
                }
            }
            File.AppendAllText(filePath, sb.ToString());
        }


        public void CreateDistanceMatrix()
        {

            float distance = 0;

            List<string> langList = GetAllUniqueLanguages();

            //Console.WriteLine("Multiple languages");
            foreach (var i in langList)
            {
                foreach (var j in langList)
                {
                    //Console.WriteLine("Check if pair: " + i + " <-> " + j + " exist in distanceMatrix");
                    if (!(languageMatrix.ContainsKey(new Tuple<string, string>(i, j)) ||
                          languageMatrix.ContainsKey(new Tuple<string, string>(j, i))))
                    {
                        List<NGramEntryIPA> sortedLang_0 = GetAllSortedByCounts(i);
                        List<NGramEntryIPA> sortedLang_1 = GetAllSortedByCounts(j);

                        //Calculate distance
                        int n = 0;
                        int m = 0;

                        foreach (var nGramEntry_0 in sortedLang_0)
                        {
                            foreach (var nGramEntry_1 in sortedLang_1)
                            {
                                if (nGramEntry_0.NgramIPA.Equals(nGramEntry_1.NgramIPA))
                                {
                                    if (n > m)
                                    {
                                        distance += n - m;
                                        m = 0;
                                        break;
                                    }
                                    else if (m > n)
                                    {
                                        distance += m - n;
                                        m = 0;
                                        break;
                                    }
                                    else if (n == m)
                                    {
                                        m = 0;
                                        break;
                                    }
                                }
                                else
                                {
                                    m++;
                                }

                            }
                            distance += m;
                            n++;
                            m = 0;
                        }

                        Console.WriteLine("lang1: " + sortedLang_0.Count);
                        Console.WriteLine("lang2: " + sortedLang_1.Count);
                        Console.WriteLine("Distance: " + distance);

                        distance = 1 - (float)(distance / (sortedLang_0.Count * sortedLang_1.Count));

                        languageMatrix.Add(new Tuple<string, string>(i, j), distance);
                        if (!i.Equals(j))
                            languageMatrix.Add(new Tuple<string, string>(j, i), distance);
                        Form1.DebugPrint("ADDED TO distance MATRIX: " + new Tuple<string, string>(i, j) +
                                         " WITH DISTANCE: " + distance);
                        if (!i.Equals(j))
                            Form1.DebugPrint("ADDED TO distance MATRIX: " + new Tuple<string, string>(j, i) +
                                             " WITH DISTANCE: " + distance);
                        distance = 0;
                    }

                }
            }
        }

    }
}