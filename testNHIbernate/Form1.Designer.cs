﻿namespace testNHIbernate
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.showDataBase = new System.Windows.Forms.Button();
            this.updateDatabaseButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.createNGram = new System.Windows.Forms.Button();
            this.consoleTextBox = new System.Windows.Forms.RichTextBox();
            this.ngramTabView = new System.Windows.Forms.TabControl();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // showDataBase
            // 
            this.showDataBase.Location = new System.Drawing.Point(3, 40);
            this.showDataBase.Name = "showDataBase";
            this.showDataBase.Size = new System.Drawing.Size(229, 31);
            this.showDataBase.TabIndex = 8;
            this.showDataBase.Text = "Show Database";
            this.showDataBase.UseVisualStyleBackColor = true;
            this.showDataBase.Click += new System.EventHandler(this.showDataBase_Click);
            // 
            // updateDatabaseButton
            // 
            this.updateDatabaseButton.Location = new System.Drawing.Point(3, 3);
            this.updateDatabaseButton.Name = "updateDatabaseButton";
            this.updateDatabaseButton.Size = new System.Drawing.Size(229, 31);
            this.updateDatabaseButton.TabIndex = 14;
            this.updateDatabaseButton.Text = "Update database!";
            this.updateDatabaseButton.UseVisualStyleBackColor = true;
            this.updateDatabaseButton.Click += new System.EventHandler(this.updateDatabaseButton_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.updateDatabaseButton, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.showDataBase, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.createNGram, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.consoleTextBox, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.ngramTabView, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(846, 650);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // createNGram
            // 
            this.createNGram.Location = new System.Drawing.Point(3, 77);
            this.createNGram.Name = "createNGram";
            this.createNGram.Size = new System.Drawing.Size(229, 31);
            this.createNGram.TabIndex = 20;
            this.createNGram.Text = "Create N-Gram";
            this.createNGram.UseVisualStyleBackColor = true;
            this.createNGram.Click += new System.EventHandler(this.createNGram_Click);
            // 
            // consoleTextBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.consoleTextBox, 3);
            this.consoleTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.consoleTextBox.Location = new System.Drawing.Point(3, 447);
            this.consoleTextBox.Name = "consoleTextBox";
            this.consoleTextBox.Size = new System.Drawing.Size(840, 200);
            this.consoleTextBox.TabIndex = 19;
            this.consoleTextBox.Text = "";
            // 
            // ngramTabView
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.ngramTabView, 2);
            this.ngramTabView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ngramTabView.Location = new System.Drawing.Point(238, 3);
            this.ngramTabView.Name = "ngramTabView";
            this.tableLayoutPanel1.SetRowSpan(this.ngramTabView, 3);
            this.ngramTabView.SelectedIndex = 0;
            this.ngramTabView.Size = new System.Drawing.Size(605, 438);
            this.ngramTabView.TabIndex = 21;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(846, 650);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "NGram Creator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button showDataBase;
        private System.Windows.Forms.Button updateDatabaseButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button createNGram;
        private System.Windows.Forms.RichTextBox consoleTextBox;
        private System.Windows.Forms.TabControl ngramTabView;
    }
}

